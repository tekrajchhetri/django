#-*- coding: utf-8 -*-
from django.shortcuts import render
# Create your views here.
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from mailer.models import Company,custom_firstrow_sql,custom_subdetail_sql




def IndexView(request):
	page = request.GET.get('page', 1)

	subdata = custom_subdetail_sql()


	start = 0
	end = 0
	if page == '1':
		start = int(page)
		end = 100
	else:
		start = int(page) * 100
		end = start+100
	data = custom_firstrow_sql()
	totalpage = int(len(data) / 100)
	data = data[start:end]

	return  render(request, "mailer/home.html",{"company_list":data, "company_data": subdata, "totalpages":range(totalpage), "currentpage":page})