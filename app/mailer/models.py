#-*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.db import connection


from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

class Company(models.Model):
    name = models.CharField(max_length=150)
    bic = models.CharField(max_length=150, blank=True)


class Contact(models.Model):
    company = models.ForeignKey(
        Company, related_name="contacts", on_delete=models.PROTECT)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, blank=True)
    email = models.EmailField()


@python_2_unicode_compatible
class Order(models.Model):
    order_number = models.CharField(max_length=150)
    company = models.ForeignKey(Company, related_name="orders")
    contact = models.ForeignKey(Contact, related_name="orders")
    total = models.DecimalField(max_digits=18, decimal_places=9)
    order_date = models.DateTimeField(null=True, blank=True)
    # for internal use only
    added_date = models.DateTimeField(auto_now_add=True)
    # for internal use only
    modified_date = models.DateTimeField(auto_now=True)



def custom_firstrow_sql():
    cursor = connection.cursor()

    data = cursor.execute("SELECT mailer_company.id as id, mailer_company.name, COUNT(mailer_order.id) as ocount, SUM(mailer_order.total) as ototal FROM mailer_company, mailer_order WHERE mailer_company.id = mailer_order.company_id GROUP BY mailer_order.company_id ORDER BY id")
    return namedtuplefetchall(data)

def custom_subdetail_sql():
    cursor = connection.cursor()
    data = cursor.execute("SELECT mailer_contact.first_name as fname, mailer_contact.company_id as cid, mailer_contact.last_name as lname, count(mailer_contact.id) as ordercount from mailer_contact, mailer_order WHERE mailer_contact.id = mailer_order.contact_id GROUP BY mailer_order.contact_id ORDER BY cid")
    return namedtuplefetchall(data)
